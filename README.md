## NB! This is a dead project! It's not supported anymore, so there is no guarantee that it will work on latest versions of browser.

# Short Description
Extension opens URL with one parameter in new tab.

It's useful for people who have to open one URL with different parameter values (for example, some prototype with different page ID-s).

[Same extension in Chrome](https://gitlab.com/irina-ivanova-extensions/URL-With-Parameter-Opener "Same extension of Chrome")

# Download and Install
* **[Install](https://addons.mozilla.org/en-US/firefox/addon/url-with-parameter-opener/ "Install")** last version from Mozilla Add-ons Store

# Questions and Comments
Any questions or comments are welcome! You can write me an e-mail on [irina.ivanova@protonmail.com](mailto:irina.ivanova@protonmail.com "irina.ivanova@protonmail.com") or create an issue here.

# Description
Basically extension simply adds your parameter to specified URL and opens it in new tab:

`URL + PARAMETER`

Where `URL` is parameter, that user should specify in Options page (only one time after installation) and `PARAMETER` is parameter that user inserts into extension field.

**Features**
* `PARAMETER` is case insensitive
* All spaces in the beginning and in the end will be trimmed
* Skype formatting will be trimmed:
   `[16.09.2014 13:34:34] Irina Ivanova: PARAMETER`
   will be recognised as `PARAMETER`
* Shows history of last 5 inserted values.

# Posts About JIRA Issue Opener
* *January 5, 2015* [Getting Started With FireFox Extensions](http://ivanova-irina.blogspot.com/2015/02/getting-started-with-firefox-extensions.html "Getting Started With FireFox Extensions")
* *September 20, 2014* [URL With Parameter Opener v1.0](http://ivanova-irina.blogspot.com/2014/09/url-with-parameter-opener-v10.html "URL With Parameter Opener v1.0")